/**
 * Función para cambiar la contraseña
 * @param event Recibe el evento
**/
function change_password(event) {
    event.preventDefault();
    if($('#id_old_password').val()=='' || $('#id_new_password').val()=='' || $('#id_new_password_repeat').val()==''){
        MaterialDialog.alert('Debes llenar todos los campos',{'title':'Alerta'});
    }
    else if($('#id_new_password').val()!==$('#id_new_password_repeat').val()){
        MaterialDialog.alert('Las contraseñas no coinciden',{'title':'Alerta'});
    }
    else{
        $.ajax({
            data: $('.change_form').serialize(), 
            type: 'POST',
            url: URL_CHANGE_PASSWORD,
            success: function(response) {
                if (response.success) {
                    MaterialDialog.alert(response.mensaje,{'title':'Éxito'});
                }
                else{
                    MaterialDialog.alert(response.mensaje,{'title':'Error'});
                }
            }
        });
    } 
}

/**
 * Función para cambiar la contraseña
 * @param user_id Recibe el usuario que recibe
 * @param user_logged Recibe el usuario logeado
**/
function get_comment(user_id,user_logged){
    var datos = {
        "csrfmiddlewaretoken":token,
        "user_recibe":user_logged,
        "user_emite":user_id,
    }
    $.ajax({
        type: 'GET',
        url: URL_LIST_COMMENT+'?user_reci='+user_id,
        success: function(response) {
            var data = JSON.parse(response);
            var html = '';
            $.each(data,function(key,value){
                var send = value.fields.visto ? 'done_all' :'done';
                var comment = value.fields.visible ? value.fields.comentario :'Este mensaje fue eliminado';
                html += '<div class="row" onclick="select_chat(this)" data-comment="'+value.pk+'">';
                if(value.fields.fk_user_emite==user_id){
                    html += '<div class="recived left">'+comment;
                }
                else{
                    html += '<div class="sended right">'+comment;
                    send = 'done_all';
                }
                if(value.fields.visible){
                    html += '<br/><i class="tiny material-icons prefix">'+send+'</i> | ';
                    html += '<small>'+ value.fields.fecha +'</small>';
                }
                html += '</div></div>';
            });
            $('.chat-box').show();
            socket.emit('update read msj',datos);
            $('.chat-log').html(html);
            $('#user_to').val(user_id);
            // bar posicion final
            $('.chat-log').scrollTop(9999999)
            
        }
    });
}

/**
 * Función para marcar como seleccionado un chat
 * @param elemento Recibe el elemento que se dió click
**/
function select_chat(elemento){
    var element = $(elemento).attr('class').split(' ');
    var test = false;
    $.each(element,function(key,value){
        test = value == 'selected' ? true:test;
    });
    if(test){
        $(elemento).removeClass('selected');
    }
    else{
        $(elemento).addClass('selected');
    }
    var selected = $('.selected');
    if(selected.length>0){
        var html = '<div class="col m12 delbutton" onclick="delete_elements()">';
        html += '<a class="btn-floating btn-large">';
        html += '<i class="tiny material-icons prefix">delete</i></a>';
        if($('.delbutton').length==0){
            $('.chat-box').prepend(html);
        }
    }
    else{
        $('.delbutton').remove();
    }
}

/**
 * Función para eliminar elementos del chat
**/
function delete_elements(){
    var comments = []
    $.each($('.selected'),function(key,value){
        comments.push($(value).attr('data-comment'))
    });
    var token = $('input').val();
    var datos = {
        "csrfmiddlewaretoken":token,
        "id_comentario":comments,
    }
    socket.emit('delete comment',datos);
}