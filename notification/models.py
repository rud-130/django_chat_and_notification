"""
Django Realtime Chat & Notifications
"""
## @package notification.models
#
# Modelos de la aplicación chat
# @version 1.0

from django.contrib.auth.models import User
from django.db import models


class TipoNotificacion(models.Model):
    """!
    Clase que contiene los datos  del tipo de Notificacion

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail)
    @copyright MIT
    @date 07-03-2018
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('nombre',)
        verbose_name = 'Tipo de Notificacion'
        verbose_name_plural = 'Tipos de Notificaciones'
        db_table = 'notificacion_tipo_notificacion'

    def __str__(self):
        """!
        Función que muestra el nombre

        @return Devuelve el nombre
        """
        return self.nombre


class Notificacion(models.Model):
    """!
    Clase que contiene los datos de las Notificaciones

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail)
    @copyright MIT
    @date 07-03-2018
    @version 1.0.0
    """
    fk_tip_notificacion = models.ForeignKey(TipoNotificacion)
    fk_user = models.ForeignKey(User)
    fecha = models.DateTimeField(auto_now_add=True)
    mensaje = models.TextField()
    visible = models.BooleanField(default=True)
    visto = models.BooleanField(default=False)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('mensaje',)
        verbose_name = 'Notificacion'
        verbose_name_plural = 'Notificaciones'
        db_table = 'notificacion_notificacion'

    def __str__(self):
        """!
        Función que muestra el mensaje

        @return Devuelve el mensaje
        """
        return self.mensaje
