"""
Django Realtime Chat & Notifications
"""
## @package notification.urls
#
# Urls de la aplicación notification
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^listar-notificaciones$', ListNotification.as_view(), name="listar_notificaciones"),
    url(r'^notificar$', AddNotification.as_view(), name="notificar"),
]
