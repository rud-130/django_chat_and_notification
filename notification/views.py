"""
Django Realtime Chat & Notifications
"""
## @package notificaction.views
#
# Vistas correspondientes a la aplicación chat
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0
import json
from django.core import serializers
from django.contrib.auth.models import User
from django.http import  JsonResponse, HttpResponse
from django.views import View

from .models import Notificacion, TipoNotificacion


class AddNotification(View):
    """!
    Clase que gestiona el agregardo de las Notificaciones

    @date 09-03-2018
    @version 1.0.0
    """
    def post(self, request, **kwargs):
        mensaje = request.POST.get('mensaje')
        usuario_recibe = request.POST.get('user_recibe')
        tipo_notificacion = request.POST.get('tipo_notificacion')
        try:
            user_rec = User.objects.get(pk=usuario_recibe)
            tipo_not = TipoNotificacion.objects.get(pk=tipo_notificacion)
        except Exception as e:
            print(e)
            user_rec = None
            tipo_not = None
        serial_noti = None
        if tipo_notificacion is not None and usuario_recibe is not None:
            try:
                notificacion = Notificacion()
                notificacion.fk_tip_notificacion = tipo_not
                notificacion.fk_user = user_rec
                notificacion.mensaje = mensaje
                notificacion.save()
                notificacion_count = Notificacion.objects.filter(
                                                    fk_user=usuario_recibe,
                                                    visible=True,
                                                    visto=False).count()
                serialized_object = serializers.serialize('json', [notificacion])
                serial_noti = json.loads(serialized_object)
                serial_noti[0]['count_notificacion'] = notificacion_count
            except Exception as e:
                print(e)
                serial_noti = {'error': 'Fallo el envio de notificacion'}

        return JsonResponse(serial_noti, safe=False)


class ListNotification(View):
    """!
    Clase que gestiona la lista de las notificaciones

    @date 09-03-2018
    @version 1.0.0
    """
    def get(self, request, **kwargs):
        notificacion = Notificacion.objects.filter(
                                fk_user=request.user.pk,
                                visible=True,
                                visto=False).all()

        serialized_object = serializers.serialize('json', notificacion)

        return HttpResponse(serialized_object, content_type='application/json')
