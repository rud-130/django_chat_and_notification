var http = require('http');
var server = http.createServer().listen(3000);
//var server = http.createServer().listen(3000,'192.168.18.218');
var io = require('socket.io').listen(server);
var querystring = require('querystring');
var host = 'localhost'
//var host = '192.168.18.218'


io.on('connection',function (socket) {
	socket.on('add comment',function(data){
		var values = querystring.stringify(data);
		var options = {
			hostname : host,
			port : '8000',
			path : '/chat/comentar',
			method : 'POST',
			headers : {
				'Content-Type':'application/x-www-form-urlencoded',
				'Content-Length':values.length,
				'Cookie':'csrftoken='+data.csrfmiddlewaretoken
			}
		}
		var request = http.request(options, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				io.emit('send comments', data);
			});
		});
		request.write(values);
		request.end();
	});


	socket.on('add not',function(data){
		var values = querystring.stringify(data);
		var options = {
			hostname : host,
			port : '8000',
			path : '/notification/notificar',
			method : 'POST',
			headers : {
				'Content-Type':'application/x-www-form-urlencoded',
				'Content-Length':values.length,
				'Cookie':'csrftoken='+data.csrfmiddlewaretoken
			}
		}
		var request = http.request(options, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				io.emit('send not', data);
			});
		});
		request.write(values);
		request.end();
	});

	socket.on('check msj',function(data){
		var values = querystring.stringify(data);
		var options = {
			hostname : host,
			port : '8000',
			path : '/chat/contador-comentarios',
			method : 'POST',
			headers : {
				'Content-Type':'application/x-www-form-urlencoded',
				'Content-Length':values.length,
				'Cookie':'csrftoken='+data.csrfmiddlewaretoken
			}
		}
		var request = http.request(options, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				io.emit('update msj', data);
			});
		});
		request.write(values);
		request.end();
	});

	socket.on('update read msj',function(data){
		var values = querystring.stringify(data);
		var options = {
			hostname : host,
			port : '8000',
			path : '/chat/actualizar-comentarios',
			method : 'POST',
			headers : {
				'Content-Type':'application/x-www-form-urlencoded',
				'Content-Length':values.length,
				'Cookie':'csrftoken='+data.csrfmiddlewaretoken
			}
		}
		var request = http.request(options, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				io.emit('updated msj', data);
			});
		});
		request.write(values);
		request.end();
	});

	socket.on('delete comment',function(data){
		var values = querystring.stringify(data);
		var options = {
			hostname : host,
			port : '8000',
			path : '/chat/eliminar-comentarios',
			method : 'POST',
			headers : {
				'Content-Type':'application/x-www-form-urlencoded',
				'Content-Length':values.length,
				'Cookie':'csrftoken='+data.csrfmiddlewaretoken
			}
		}
		var request = http.request(options, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				io.emit('deleted msj', data);
			});
		});
		request.write(values);
		request.end();
	});
});