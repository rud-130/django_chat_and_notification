"""
Django Realtime Chat & Notifications
"""
## @package chat.urls
#
# Urls de la aplicación chat
# @author Leonel Hernández (leonelphm at gmail)
# @author Rodrigo Boet (rudmanmrrod at gmail)
# @version 1.0
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', ChatView.as_view(), name = "chat"),

    url(r'^listar-comentarios$', ListComment.as_view(), name="listar_comentarios"),
    url(r'^comentar$', AddComment.as_view(), name="comentar"),
    url(r'^contador-comentarios$', CountNotificationMsn.as_view(), name="contador_comentarios"), 
    url(r'^actualizar-comentarios$', UpdateChatNotificacion.as_view(), name="actualizar_comentarios"),     
    url(r'^eliminar-comentarios$', DeleteLogicComment.as_view(), name="eliminar_comentarios"),
]
